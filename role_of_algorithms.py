"""The Role of the algorithms in computing."""
from math import log

# 1) Overview of all algorithms and their place in modern computing system.
# 2) Which algorithm is best for a given application depends on
# among other factors
# a) The number of items to be sorted.
# b) The extend to which the items are already somewhat sorted
# c) Possible restrictions on the item values
# d) The architecture of the computer
# e) The kind of storage dev0ices to be used.
# 3) An algorithm is said to be correct if, for every input instance,
# it halts with the correct output.


# Question1: Give a real-world example that requires sorting or
# real-world example that re-quires computing a convex hull.

# Answere: Job of the sorting is to put things in order. On internet
# information gets broken down into packets of data, which then get sent across
# web. Now, to reassamble that data, sorting algorithms are crucial to putting
# this data back in correct order.

# Question2: Select a data structure that you have seen previously, and discuss
# its strengths and limitations.
# Answere:
# 1) List
#   a) Arraylist
#    Positive
#    1) Retrieve of element O(1)
#   Negative
#    1) Insert of element at begining or middle O(n)
#  b) LinkedList
#   Positive
#   1) Insert of element at begining or middle O(1)
#  Negative
#   1) Retrieve of element O(n)


# Question 3: Come up with a real-world problem in which only the best solution
# will do. Then come up with one in which a solution that is “approximately”
# the best is good enough.

# Answere: Matching algorithms
# 1) It is used to match-up students to colleges so that every one got a place,
# but more importantly, was happy even if they didn't get their first choice.
# 2) Dating wed site.

# Question 4: Suppose we are comparing implementations of insertion sort and
# merge sort on the same machine. For inputs of size n, insertion sort runs in
# 8n^2 steps, while merge sort runs in 64*n*lg(n) steps. For which values of
# n does insertion sort beat merge sort?

def isInsertionBeatMerge(arg):
    """Compare insertion sort with merge sort."""
    return (8 * pow(arg, 2)) < (64 * arg * log(arg, 2))


def getInsertionMergeBreakPoint():
    """Get break point between insertion and merge."""
    for nelements in range(2, 100):
        if not isInsertionBeatMerge(nelements):
            return nelements


getInsertionMergeBreakPoint()

# Ans: 44

# Question 5:
for i in range(2, 100):
    if (100 * pow(i, 2)) < pow(2, i):
        print(i)
        break

# Ans: 15

# Chapter 2:


def insertionSortNondecreasing(lst):
    """Sort the input list in asscending order."""
    listLength = len(lst)
    for j in range(2, listLength):
        key = lst[j]
        i = j - 1
        while i >= 0 and lst[i] > key:
            lst[i+1] = lst[i]
            i = i - 1
        lst[i+1] = key
    return lst


# Question1: illustrate the operation of insertion sort on the Arraylist
# A = [31, 41, 59, 26, 41, 58]
insertionSortNondecreasing([31, 41, 59, 26, 41, 58])

# Question2: Re-write the insertion sort procedure to sort into nonincreasing
# instead of non-decreasing order


def insertionSortNonincreasing(lst):
    """Sort the input list in descending order."""
    listLength = len(lst)
    for j in range(2, listLength):
        key = lst[j]
        i = j - 1
        while i >= 0 and lst[i] < key:
            lst[i+1] = lst[i]
            i = i - 1
        lst[i+1] = key
    return lst


insertionSortNonincreasing([])

# Question 3: Pseudocode for linear search
# Answere:
# LINEARSEARCH(A, v)
#     result = NIL
#     for j = 1 to A.length
#         if A[j] == v
#             return j
#     return result


def linearSearch(A, v):
    """To get index of v in list A."""
    result = None
    listLength = len(A)
    for j in range(listLength):
        if A[j] == v:
            return j
    return result


linearSearch([31, 41, 59, 26, 41, 58], 41)


# @profile
def selectionSort(lst):
    """Sort the input list in descending order."""
    listLength = len(lst)
    for i in range(listLength-1):
        min = i
        for j in range(i+1, listLength):
            if lst[min] > lst[j]:
                min = j
        temp = lst[min]
        lst[min] = lst[i]
        lst[i] = temp
    return lst


# selectionSort([31, 41, 59, 26, 41, 58])
# selectionSort([26, 31, 41, 41, 58, 59])
selectionSort([59, 58, 41, 41, 31, 26])
pow(2, pow(10, 6)) > pow(10, 6)
log(pow(10, 6), 2)
