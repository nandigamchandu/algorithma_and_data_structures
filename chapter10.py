"""Chapter 10."""


class Stack:
    """Implement stack class."""

    def __init__(self, size):
        """To initialize size and top of the stack."""
        self.stack = [None] * size
        self.top = 0

    def push(self, element):
        """To add element at top of the stack."""
        if self.top == len(self.stack):
            raise Exception("overflow")
        self.stack[self.top] = element
        self.top += 1

    def pop(self):
        """Return top element from the stack."""
        if self.isEmpty():
            raise Exception("underflow")
        else:
            self.top -= 1
            topElement = self.stack[self.top]
            self.stack[self.top] = None
        return topElement

    def isEmpty(self):
        """Return True if stack is empty."""
        if self.top == 0:
            return True
        return False


# 10.1-1
S = Stack(6)
S.push(4)
S.push(1)
S.push(3)
S.pop()
S.push(8)
S.push(8)
S.push(8)
S.pop()


class Queue:
    """Implement queue class."""

    def __init__(self, size):
        """To initialize the size, head and tail of the Queue."""
        self.queue = [None] * size
        self.tail = 0
        self.head = 0

    def deQueue(self):
        """Return head element from the queue."""
        if self.head == self.tail and self.queue[self.head+1] is None:
            raise Exception("underflow")
        x = self.queue[self.head]
        self.queue[self.head] = None
        if self.head == len(self.queue) - 1:
            self.head = 0
        else:
            self.head += 1
        return x

    def enQueue(self, element):
        """To add element at tail of the queue."""
        if (self.head == self.tail) and self.queue[self.head+1] is not None:
            raise Exception("overflow")
        self.queue[self.tail] = element
        if self.tail == len(self.queue) - 1:
            self.tail = 0
        else:
            self.tail += 1


Q = Queue(6)
Q.enQueue(4)
Q.enQueue(1)
Q.enQueue(3)
Q.deQueue()
Q.enQueue(8)
Q.deQueue()


class twoStackOneArray:
    """Implement two stacks with one array."""

    def __init__(self, size):
        """To initialize the size of array and tops of stacks."""
        self.stack = [None] * size
        self.top1 = 0
        self.top2 = size - 1

    def pushStackOne(self, element):
        """To add element at top of the frist stack."""
        if self.top1 > self.top2:
            raise Exception("stack one is overflow")
        self.stack[self.top1] = element
        self.top1 += 1

    def pushStackTwo(self, element):
        """To add element at top of the second stack."""
        if self.top1 > self.top2:
            raise Exception("stack two is overflow")
        self.stack[self.top2] = element
        self.top2 -= 1

    def popStackOne(self):
        """Return top element from the frist stack."""
        if self.top1 == 0:
            raise Exception("stack one is underflow")
        self.top1 -= 1
        topElement = self.stack[self.top1]
        self.stack[self.top1] = None
        return topElement

    def popStackTwo(self):
        """Return top element from the second stack."""
        if self.top2 == len(self.stack-1):
            raise Exception("stack two is underflow")
        self.top2 += 1
        topElement = self.stack[self.top2]
        self.stack[self.top2] = None
        return topElement


S = twoStackOneArray(8)
S.pushStackOne(2)
S.pushStackTwo(3)
S.popStackOne()


class QueueTwoStack:
    """Implement queue using two stack."""

    def __init__(self, size):
        """To initialize Two stacks and size of the queue."""
        self.stack1 = Stack(size)
        self.stack2 = Stack(size)

    def enQueue(self, element):
        """To add element at tail of the queue."""
        self.stack1.push(element)

    def deQueue(self):
        """Return head element from the queue."""
        self.moveElementsFromStack1ToStack2()
        element = self.stack2.pop()
        self.moveElementsFromStack2ToStack1()
        return element

    def moveElementsFromStack1ToStack2(self):
        """To move element from stack1 to stack2."""
        while True:
            try:
                self.stack2.push(self.stack1.pop())
            except Exception:
                break

    def moveElementsFromStack2ToStack1(self):
        """To move element from stack2 to stack1."""
        while True:
            try:
                self.stack1.push(self.stack2.pop())
            except Exception:
                break


queueWithTwoStacks = QueueTwoStack(6)
queueWithTwoStacks.enQueue(5)
queueWithTwoStacks.enQueue(4)
queueWithTwoStacks.enQueue(3)
queueWithTwoStacks.enQueue(2)
queueWithTwoStacks.deQueue()
queueWithTwoStacks.deQueue()
queueWithTwoStacks.enQueue(71)
queueWithTwoStacks.enQueue(11)
queueWithTwoStacks.deQueue()
queueWithTwoStacks.deQueue()
queueWithTwoStacks.deQueue()
