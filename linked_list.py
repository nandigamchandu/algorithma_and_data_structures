"""Implement Linked List."""


class Node:
    """Node implementation for doubly-linked list."""

    def __init__(self, v=None, next=None, prev=None):
        """Construct a node."""
        self.key = v
        self.next = next
        self.prev = prev


class DoubleLinkedList:
    """Implementation of doubly-linked list."""

    def __init__(self):
        """Construct a doubly-linked list."""
        self.head = None
        self.tail = None
        self.length = 0

    def isEmpty(self):
        """Return True of linked list is empty."""
        if self.length == 0:
            return True
        return False

    def prepend(self, key):
        """To insert element into list."""
        if self.isEmpty():
            self.insertFristNode(key)
        else:
            newCell = Node(key, self.head, None)
            self.head.prev = newCell
            self.head = newCell
            self.length += 1

    def insertFristNode(self, key):
        """To insert new key."""
        newCell = Node(key, self.head, None)
        self.head = newCell
        self.tail = newCell
        self.length = 1

    def simpleTraverse(self):
        """Traverse and print only node values."""
        node = self.head
        print('Head->', end='')
        while node is not None:
            print(f"{node.key}->", end='')
            node = node.next
        print('Tail')

    def remove(self, index):
        """To remove key at given index."""
        if index <= 0 or index > self.length:
            return -1
        node = self.head
        for i in range(1, index+1):
            if i == 1 and i == index:
                node.next.prev = None
                self.head = node.next
            elif i > 1 and i < self.length and i == index:
                node.next.prev = node.prev
                node.prev.next = node.next
            elif i == self.length and i == index:
                node.prev.next = None
                self.tail = node.prev
            node = node.next
        self.length -= 1

    def seekFromHead(self, position):
        """Return key present at given position from head of the linkedlist."""
        if position < 1 or position > self.length:
            return -1
        node = self.head
        for i in range(1, position):
            node = node.next
        return node.key

    def binarySearch(self, key):
        """To search key in double linked list."""
        left = 0
        right = self.length
        while (right >= left):
            middle = (left + right) // 2
            if key == self.seekFromHead(middle) and middle > 0:
                return middle
            elif key < self.seekFromHead(middle):
                right = middle - 1
            else:
                left = middle + 1
        return -1


dlist = DoubleLinkedList()
dlist.prepend(100)
dlist.prepend(92)
dlist.prepend(75)
dlist.prepend(32)
dlist.prepend(25)
dlist.prepend(20)
dlist.prepend(10)
dlist.prepend(5)
dlist.prepend(2)
dlist.simpleTraverse()
dlist.binarySearch(20)
dlist.binarySearch(86)
dlist.binarySearch(-1)
dlist.binarySearch(2)
dlist.binarySearch(1)
