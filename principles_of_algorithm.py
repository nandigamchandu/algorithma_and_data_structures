"""Principles of algorithm analysis."""
import math
import matplotlib.pyplot as plt
import pandas as pd
import timeit
plt.rcParams['figure.figsize'] = 8, 8


# 2.2
# @profile
def myCount(n):
    """Counter."""
    count = 0
    for i in range(n):
        for j in range(n):
            for k in range(n):
                count += 1
    return count


timeit.timeit('myCount(10)', number=1, globals=globals())
# Ans 7.569e-05
timeit.timeit('myCount(100)', number=1, globals=globals())
# Ans 52.07ms
timeit.timeit('myCount(1000)', number=1, globals=globals())
# Ans 47.05 sec


def nCounter(total_count, step):
    """Counter."""
    count = 0
    while True:
        for i in range(step):
            for j in range(step):
                for k in range(step):
                    if count >= total_count:
                        print(count)
                        return count
                    count += 1
    return count


timeit.timeit('nCounter(pow(10, 9), 10)', number=1, globals=globals())
# Ans 1min 9s
timeit.timeit('nCounter(pow(10, 9), 100)', number=1, globals=globals())
# Ans 46.9 s
timeit.timeit('nCounter(pow(10, 9), 1000)', number=1, globals=globals())
# Ans 58 s


def countBillion():
    """To count billon."""
    count = 0
    while count <= pow(10, 9):
        count += 1
    return count


timeit.timeit('countBillion()', number=1, globals=globals())
# Ans 1min 16s
#
# %%
# 2.5 For what values of N is 10NlgN > 2N^2?
df = pd.DataFrame(list(range(1, 100)), columns=['N'])
df['10NlgN'] = df['N'].apply(lambda x: 10 * x * math.log(x, 2))
df['2N^2'] = df['N'].apply(lambda x: 2 * pow(x, 2))
plt.plot(df['N'], df['10NlgN'], label='10NlgN')
plt.plot(df['N'], df['2N^2'], label='2N^2')
plt.xlabel('N')
plt.legend()

# %%
# 2.6 For what values of N is N^(3/2) between (N(lgN)^2)/2 and 2N(lgN)^2
df = pd.DataFrame(list(range(1, 7)), columns=['N'])
df['(N(lgN)^2)/2'] = df['N'].apply(lambda x: (x * (math.log(x, 2)) ** 2) / 2)
df['2N(lgN)^2'] = df['N'].apply(lambda x: 2 * x * (math.log(x, 2)) ** 2)
df['N^(3/2)'] = df['N'].apply(lambda x: pow(x, 3/2))
plt.plot(df['N'], df['(N(lgN)^2)/2'], label='(N(lgN)^2)/2')
plt.plot(df['N'], df['2N(lgN)^2'], label='2N(lgN)^2')
plt.plot(df['N'], df['N^(3/2)'], label='N^(3/2)')
plt.xlabel('N')
plt.legend()

# %%
# 2.7 For what values of N is 2NH - N < NlgN + 10N?


def harmonicNumber(n):
    """To get approximate harmonic number."""
    GAMMA = 0.57721
    return math.log(n) + GAMMA + (1 / (2 * n))


df = pd.DataFrame(list(range(1, 10)), columns=['N'])
df['2NH-N'] = df['N'].apply(lambda x: 2 * x * harmonicNumber(x) - x)
df['NlgN+10N'] = df['N'].apply(lambda x: x * math.log(x, 2) + 10 * x)
plt.plot(df['N'], df['2NH-N'], label='2NH-N')
plt.plot(df['N'], df['NlgN+10N'], label='NlgN+10N')
plt.xlabel('N')
plt.legend()

# %%
# 2.8 what is the smallest value of N for loglogN > 8 ?
# Ans 10^10^9

# %%
# 2.9 Prove that floor(lgN) + 1 is the number of bits required to represent
# N in binary


def nbits(n):
    """To get number of bits require to store a decimal number."""
    return math.floor(math.log(n, 2)) + 1


nbits(8)

# %%
# 2.14 How many digits are there in the decimal representation of 1 million?


def ndigits(n):
    """To get number of digits in a number."""
    return math.floor(math.log(n, 10)) + 1


# ndigits(factorial(pow(10, 6)))
# Ans 5565709

# %%
# 2.18 Give the smallest values of N for which floor(H) = i for 1 <= i <= 10?
df = pd.DataFrame(list(range(2, 100000)), columns=['N'])
df['floor(H)'] = df['N'].apply(lambda x: math.floor(math.log(x) + 0.57721))
df = df[(df['floor(H)'] >= 1) & (df['floor(H)'] <= 10)]
(df.groupby(['floor(H)'])
   .agg({'N': min})
   .rename(columns={'N': 'smallest N value'}))


nbits(pow(10, 6))
# Binary search


def binarySearch(lst, value, left, right):
    """To get index of a value in the list."""
    while (right >= left):
        middle = (left + right) // 2
        if value == lst[middle]:
            return middle
        if (value < lst[middle]):
            right = middle - 1
        else:
            left = middle + 1
    return -1


def sequentialSearch(lst, value):
    """To get index of a value in the list."""
    for i in range(lst):
        if value == lst[i]:
            return i
    return -1


a = [1, 8, 9, 5, 6, 7, 12, 20, 40]
a = sorted(a)
a
binarySearch(a, 40, 0, len(a))

# %%
df = pd.DataFrame(list(range(1, 50)), columns=['N'])
df['lgN'] = df['N'].apply(lambda x: math.log(x, 2))
df['C'] = 100
df['NlgN'] = df['N'].apply(lambda x: x * math.log(x, 2))
df['N^3/2'] = df['N'].apply(lambda x:  pow(x, 3/2))
df['N^2'] = df['N'].apply(lambda x:  pow(x, 2))
plt.plot(df['N'], df['C'], label='C')
plt.plot(df['N'], df['lgN'], label='lgN')
plt.plot(df['N'], df['NlgN'], label='NlgN')
plt.plot(df['N'], df['N^3/2'], label='N^3/2')
plt.plot(df['N'], df['N^2'], label='N^2')
plt.xlabel('N')
plt.legend()

# %%
df = pd.DataFrame(list(range(1, 500)), columns=['N'])
df['N^2+40N+300'] = df['N'].apply(lambda x: pow(x, 2) + 40 * x + 300)
df['N^2'] = df['N'].apply(lambda x: pow(x, 2))
plt.plot(df['N'], df['N^2+40N+300'], label='N^2+40N+300')
plt.plot(df['N'], df['N^2'], label='N^2')
plt.xlabel('N')
plt.legend()
