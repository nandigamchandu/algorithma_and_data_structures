"""Sorting in Linear Time."""
import math
import itertools


# Counting Sort
def countingSort(A, k):
    """
    Return sorted list copy.

    It is one of stable sorting algorithm.
    It assumes that input list consists of integer in a small range.
    """
    B = [None] * len(A)
    C = [0] * (k+1)
    for j in range(len(A)):
        C[A[j]] += 1
    for i in range(1, k+1):
        C[i] = C[i] + C[i-1]
    for j in range(len(A)-1, -1, -1):
        B[C[A[j]]-1] = A[j]
        C[A[j]] = C[A[j]] - 1
    return B


A = [2, 5, 3, 0, 2, 3, 0, 3]
countingSort(A, max(A))
A = [6, 0, 2, 0, 1, 3, 4, 6, 1, 3, 2]
countingSort(A, max(A))

# Radix Sort
A = ['COW', 'DOG', 'SEA', 'RUG', 'ROW', 'MOB', 'BOX', 'TAB',
     'BAR', 'EAR', 'TAR', 'DIG', 'BIG', 'TEA', 'NOW', 'FOX']


def insertionSortPlaceBase(lst, place):
    """Return sorted list base on place."""
    listLength = len(lst)
    for j in range(1, listLength):
        key = lst[j]
        i = j - 1
        while i >= 0 and lst[i][place] > key[place]:
            lst[i+1] = lst[i]
            i = i - 1
        lst[i+1] = key
    return lst


def radixSort(A, d):
    """To sort elements in the list."""
    for i in range(d-1, -1, -1):
        A = insertionSortPlaceBase(A, i)


radixSort(A, 3)


# Bucket Sort
def insertionSort(lst):
    """To sort the input list in asscending order."""
    listLength = len(lst)
    for j in range(1, listLength):
        key = lst[j]
        i = j - 1
        while i >= 0 and lst[i] > key:
            lst[i+1] = lst[i]
            i = i - 1
        lst[i+1] = key


def bucketSort(A):
    """
    Return sorted list.

    This sorting algorithm is applied on uniformly distributed values whose
    values are in range of 0 <= value < 1.
    """
    B = [None] * len(A)
    n = len(A)
    for i in range(len(A)):
        B[i] = list()
    for i in range(n):
        B[math.floor(n*A[i])].append(A[i])
    for i in range(n):
        insertionSort(B[i])
    return list(itertools.chain.from_iterable(B))


A = [0.79, 0.16, 0.13, 0.64, 0.39, 0.20, 0.89, 0.53, 0.71, 0.42]
bucketSort(A)

# water jugs
red_jugs = [5, 8, 9, 1, 6, 3, 2, 4, 7]
blue_jugs = [5, 9, 6, 7, 8, 4, 2, 3, 1]


def groupSameElements(A, B):
    """To group list A and B."""
    for i in range(len(A)):
        for j in range(i, len(B)):
            if A[i] == B[j]:
                B[i], B[j] = B[j], B[i]
                break
    return list(zip(A, B))


groupSameElements(red_jugs, blue_jugs)
