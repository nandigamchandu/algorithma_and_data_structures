"""Chapter 7 Quick sort."""
import random
import math
import timeit


def partitioning(A, p, r):
    """To partition the list."""
    x = A[r]
    i = p - 1
    for j in range(p, r):
        if A[j] <= x:
            i = i + 1
            A[i], A[j] = A[j], A[i]
    A[i+1], A[r] = A[r], A[i+1]
    return i + 1


def quickSort(A, p, r):
    """To sort list."""
    if p < r:
        q = partitioning(A, p, r)
        quickSort(A, p, q-1)
        quickSort(A, q+1, r)


A = [1, 2, 3, 4, 5, 6, 7]
quickSort(A, 0, len(A)-1)


# 7.1-2
def partitioningSameValue(A, p, r):
    """To partition the list have same value as elements."""
    partitioning(A, p, r)
    return math.floor((p+r)/2)


def quickSortSameValue(A, p, r):
    """To sort the list have same value as elements."""
    if p < r:
        q = partitioningSameValue(A, p, r)
        quickSortSameValue(A, p, q-1)
        quickSortSameValue(A, q+1, r)


A = [4] * 1000
timeit.timeit("quickSort(A, 0, len(A)-1)", number=1, globals=globals())
# Ans : 0.08759952299806173
# Time complexity is O(n^2)
(timeit.timeit("quickSortSameValue(A, 0, len(A)-1)", number=1,
               globals=globals()))
# Ans : 0.011749529003282078
# Time complexity is O(nlgn)


# 7.1-4
def partitioningNonincreasing(A, p, r):
    """To partition the list."""
    x = A[r]
    i = p - 1
    for j in range(p, r):
        if A[j] >= x:
            i = i + 1
            A[i], A[j] = A[j], A[i]
    A[i+1], A[r] = A[r], A[i+1]
    return i + 1


def quickSortNonincreasing(A, p, r):
    """To sort list."""
    if p < r:
        q = partitioningNonincreasing(A, p, r)
        quickSortNonincreasing(A, p, q-1)
        quickSortNonincreasing(A, q+1, r)


A = [5, 1, 6, 8, 3, 4, 10]
quickSortNonincreasing(A, 0, len(A)-1)


# Randomized version of quicksort
def randomizedPartition(A, p, r):
    """To select pivot element randomly."""
    i = random.randint(p, r)
    A[r], A[i] = A[i], A[r]
    return partitioning(A, p, r)


def randomizedQuickSort(A, p, r):
    """To sort list."""
    if p < r:
        q = randomizedPartition(A, p, r)
        randomizedQuickSort(A, p, q-1)
        randomizedQuickSort(A, q+1, r)


A = [4, 9, 6, 7, 8, 18, 4, 6, 11, 15]
randomizedQuickSort(A, 0, len(A)-1)
A


# Hoare partition correctness
def hoarePartition(A, p, r):
    """To split the list using hoare partition."""
    x = A[p]
    i = p - 1
    j = r + 1
    while True:
        while True:
            j = j - 1
            if A[j] <= x:
                break
        while True:
            i = i + 1
            if A[i] >= x:
                break
        if i < j:
            A[i], A[j] = A[j], A[i]
        else:
            return j


def quickSortHoarePartition(A, p, r):
    """To sort list using hoare partition."""
    if p < r:
        q = hoarePartition(A, p, r)
        quickSortHoarePartition(A, p, q-1)
        quickSortHoarePartition(A, q+1, r)


A = [13, 19, 9, 5, 12, 8, 7, 4, 11, 2, 6, 21]
quickSortHoarePartition(A, 0, len(A)-1)
(timeit.timeit("quickSortHoarePartition(A, 0, len(A)-1)",
               number=1, globals=globals()))
# Ans : 0.1 microSec
A = [11] * 12
quickSortHoarePartition(A, 0, len(A)-1)
(timeit.timeit("quickSortHoarePartition(A, 0, len(A)-1)",
               number=1, globals=globals()))
# Ans : 0.4 microSec
