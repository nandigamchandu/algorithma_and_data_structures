"""Implement deque data structure."""


class Deque:
    """
    Implement deque data structure.

    This data structure gives us ability to add and delete element
    from either ends.
    """

    def __init__(self, size):
        """Initialize left, right and size of deque."""
        self.left = 0
        self.right = 0
        self.deque = [None] * size
        self.dequeFull = False
        self.dequeEmpty = True

    def insertFromRight(self, element):
        """Insert element from rigth of deque."""
        self.raiseOverflow()
        self.deque[self.right] = element
        self.rightInc()
        self.shiftLeftIndexForEmptyDeque()
        self.setFullTag()

    def insertFromLeft(self, element):
        """Insert element from left of deque."""
        self.raiseOverflow()
        self.deque[self.left] = element
        self.leftDec()
        self.shiftRigthIndexForEmptyDeque()
        self.setFullTag()

    def popFromRight(self):
        """Remove element from left of the deque."""
        self.raiseUnderFlow()
        self.rightDec()
        element = self.deque[self.right]
        self.deque[self.right] = None
        self.setEmptyTag()
        return element

    def popFromLeft(self):
        """Remove element from rigth of the deque."""
        self.raiseUnderFlow()
        self.leftInc()
        element = self.deque[self.left]
        self.deque[self.left] = None
        self.setEmptyTag()
        return element

    def setEmptyTag(self):
        """To set empty tag of deque."""
        self.dequeEmpty = self.left == self.right
        self.dequeFull = False

    def setFullTag(self):
        """To set full tag of deque."""
        self.dequeFull = (self.left == self.right)
        self.dequeEmpty = False

    def rightInc(self):
        """To increment right index."""
        if self.right == len(self.deque) - 1:
            self.right = 0
        else:
            self.right += 1

    def rightDec(self):
        """To decrement right index."""
        if self.right == 0:
            self.right = len(self.deque) - 1
        else:
            self.right -= 1

    def leftInc(self):
        """To increment left index."""
        if self.left == len(self.deque) - 1:
            self.left = 0
        else:
            self.left += 1

    def leftDec(self):
        """To decrement left index."""
        if self.left == 0:
            self.left = len(self.deque) - 1
        else:
            self.left -= 1

    def raiseUnderFlow(self):
        """Raise exception when deque is underflow."""
        if self.dequeEmpty:
            raise Exception('deque is underflow')

    def raiseOverflow(self):
        """Raise exception when deque is overflow."""
        if self.dequeFull:
            raise Exception('deque is overflow')

    def shiftLeftIndexForEmptyDeque(self):
        """To shift left index by one toward left if deque is empty."""
        if self.dequeEmpty:
            self.leftDec()

    def shiftRigthIndexForEmptyDeque(self):
        """To shift rigth index by one toward rigth if deque is empty."""
        if self.dequeEmpty:
            self.rightInc()


deq = Deque(6)
deq.insertFromLeft(1)
deq.insertFromRight(2)
deq.popFromRight()
