"""Queue implementation."""


class Queue:
    """Implement queue class."""

    def __init__(self, size):
        """To initialize the size, head and tail of the Queue."""
        self.queue = [None] * size
        self.tail = 0
        self.head = 0
        self.queueFull = False
        self.queueEmpty = True

    def deQueue(self):
        """Return head element from the queue."""
        if self.queueEmpty:
            raise Exception("underflow")
        element = self.queue[self.head]
        self.queue[self.head] = None
        if self.head == len(self.queue) - 1:
            self.head = 0
        else:
            self.head += 1
        self.queueEmpty = (self.head == self.tail)
        self.queueFull = False
        return element

    def enQueue(self, element):
        """To add element at tail of the queue."""
        if self.queueFull:
            raise Exception("overflow")
        self.queue[self.tail] = element
        if self.tail == len(self.queue) - 1:
            self.tail = 0
        else:
            self.tail += 1
        self.queueFull = (self.head == self.tail)
        self.queueEmpty = False


Q = Queue(6)
Q.enQueue(4)
Q.enQueue(1)
Q.enQueue(3)
Q.deQueue()
Q.enQueue(8)
Q.deQueue()
