"""Implement stack with two queues."""

import queue


class StackWithTwoQueues:
    """Implement stack with two queues."""

    def __init__(self, size):
        """To initialize the size of stack and the two queues."""
        self.queue1 = queue.Queue(size)
        self.queue2 = queue.Queue(size)

    def push(self, element):
        """To add element add at top of the stack."""
        self.queue1.enQueue(element)

    def pop(self):
        """Return element for top of the stack."""
        self.moveElementFromQ1ToQ2()
        element = self.queue1.deQueue()
        self.moveElementFromQ2ToQ1()
        return element

    def moveElementFromQ1ToQ2(self):
        """To move elements from queue1 to queue2."""
        while True:
            if (self.queue1.head == self.queue1.tail - 1):
                break
            self.queue2.enQueue(self.queue1.deQueue())

    def moveElementFromQ2ToQ1(self):
        """To move element from queue2 to queue1."""
        while True:
            try:
                self.queue1.enQueue(self.queue2.deQueue())
            except Exception:
                break


stackWithTwoQueues = StackWithTwoQueues(6)
stackWithTwoQueues.push(88)
stackWithTwoQueues.push(11)
stackWithTwoQueues.push(6)
stackWithTwoQueues.pop()
stackWithTwoQueues.pop()
stackWithTwoQueues.push(6)
stackWithTwoQueues.push(6)
stackWithTwoQueues.pop()
